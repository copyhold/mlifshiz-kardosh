+++
date = "2015-11-17T11:33:28+02:00"
description = "no"
title = "המרפאות שלנו"
Class = "mirpaot"
topimg="office.jpg"
+++

חברים יקרים,   
לרשותכם עומדות 2 מרפאות מושקעות, נעימות ונוחות, עם צוות קשוב ואחראי, 
והציוד המקצועי והמתקדם ביותר.

![office1](/img/office-sm-0.jpg)

מרפאת אחוזת ראשונים
--------------
רח' הנחשול 30, נווה ים, ראשון לציון

[![Rishon map](https://maps.googleapis.com/maps/api/staticmap?center=31.984413,%2034.782754&size=514x264&zoom=15&markers=color:blue|label:H|31.984413,%2034.782754)](http://is.gd/ZI0kBQ)


![office2](/img/office-sm-2.jpg)
מרפאת אסותא תל-אביב
------------
רח' הברזל 10, בניין העוגן קומה 6, 
רמת החיל ת"א
[![Tel Aviv map](https://maps.googleapis.com/maps/api/staticmap?center=32.1081205,34.835851&size=514x264&zoom=15&markers=color:blue|label:H|32.1081205,34.835851)](http://is.gd/cD3ZNp)


